package main

type Env5 struct {
	A, B   bool
	La, Lb bool
	door   bool
}

func (e *Env5) InitState(s State) {
	e.A = s.Bool("a")
	e.B = s.Bool("b")
	e.La = s.Bool("la")
	e.Lb = s.Bool("lb")
	e.door = s.Bool("door")
}

func (e *Env5) State() State {
	return State{
		"a":  e.A,
		"b":  e.B,
		"la": e.La,
		"lb": e.Lb,
	}
}

func (e *Env5) Set(action Action) {
	switch action {
	case openDoor:
		e.door = true
	case closeDoor:
		e.door = false
	case nop:
	default:
		panic("invalid action")
	}
}

type Agent5 struct {
	door  bool
	power int64
}

func (a *Agent5) Act(state State) Action {
	locA := state["a"].(bool) && state["la"].(bool)
	locB := state["b"].(bool) && state["lb"].(bool)

	if (locA || locB) && !a.door {
		a.power++
		return openDoor
	} else if !(locA || locB) && a.door {
		a.power++
		return closeDoor
	}

	return nop
}

func (a *Agent5) Performance() Performance {
	return Performance{
		"power": a.power,
	}
}
