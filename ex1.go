package main

type Env1 struct {
	A, B bool
	door bool
}

func (e *Env1) InitState(s State) {
	e.A = s.Bool("a")
	e.B = s.Bool("b")
	e.door = s.Bool("door")
}

func (e *Env1) State() State {
	return State{
		"a": e.A,
		"b": e.B,
	}
}

func (e *Env1) Set(action Action) {
	switch action {
	case openDoor:
		e.door = true
	case closeDoor:
		e.door = false
	case nop:
	default:
		panic("invalid action")
	}
}

type Agent1 struct {
	door  bool
	power int64
}

func (a *Agent1) Act(state State) Action {
	locA := state["a"].(bool)
	locB := state["b"].(bool)

	if (locA || locB) && !a.door {
		a.power++
		return openDoor
	} else if !(locA || locB) && a.door {
		a.power++
		return closeDoor
	}

	return nop
}

func (a *Agent1) Performance() Performance {
	return Performance{
		"power": a.power,
	}
}
