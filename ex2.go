package main

type Env2 struct {
	A, B bool
	door bool
}

func (e *Env2) InitState(s State) {
	e.A = s.Bool("a")
	e.B = s.Bool("b")
	e.door = s.Bool("door")
}

func (e *Env2) State() State {
	return State{
		"a":    e.A,
		"b":    e.B,
		"door": e.door,
	}
}

func (e *Env2) Set(action Action) {
	switch action {
	case openDoor:
		e.door = true
	case closeDoor:
		e.door = false
	case nop:
	default:
		panic("invalid action")
	}
}

type Agent2 struct {
	door  bool
	power int64
}

func (a *Agent2) Act(state State) Action {
	locA := state["a"].(bool)
	locB := state["b"].(bool)
	door := state["door"].(bool)

	if (locA || locB) && !door {
		a.power++
		return openDoor
	} else if !(locA || locB) && door {
		a.power++
		return closeDoor
	}

	return nop
}

func (a *Agent2) Performance() Performance {
	return Performance{
		"power": a.power,
	}
}
