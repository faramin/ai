package main

const (
	openDoor  = Action("open door")
	closeDoor = Action("close door")
	nop       = Action("nop")
)
