package main

import (
	"fmt"
	"math/rand"
)

type State map[string]interface{}

func (s State) Bool(key string) bool {
	if value, exists := s[key]; exists {
		return value.(bool)
	}
	return false
}

type RandomState map[string][]interface{}

func (rs RandomState) Choose() State {
	choosed := State{}
	for k, vs := range rs {
		choosedN := rand.Intn(len(vs))
		if choosedItem := rs[k][choosedN]; choosedItem != nil {
			choosed[k] = choosedItem
		}
	}
	return choosed
}

type Action string

type Performance map[string]int64

func (p Performance) String() string {
	s := ""
	for k, v := range p {
		s += fmt.Sprintf("%s: %d\n", k, v)
	}
	return s
}

type Environemt interface {
	InitState(State)
	State() State
	Set(Action)
}

func NewEnv(env Environemt, rs RandomState) Environemt {
	env.InitState(rs.Choose())
	return env
}

type Agent interface {
	Act(State) Action
	Performance() Performance
}

type Simulator interface {
	InjectState(State)
	Tick()
	Performance() Performance
}
