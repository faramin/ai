package main

import (
	"fmt"
	"io"
)

type DefaultSimulator struct {
	agent Agent
	env   Environemt

	logger io.Writer
}

func (s *DefaultSimulator) InjectState(state State) {
	s.env.InitState(state)
}

func (s *DefaultSimulator) Tick() {
	state := s.env.State()
	fmt.Fprintln(s.logger, "STATE", state)
	action := s.agent.Act(state)
	fmt.Fprintln(s.logger, "ACTION", action, " - ", state)
	s.env.Set(action)
}

func (s *DefaultSimulator) Performance() Performance {
	return s.agent.Performance()
}

func Simulate(simul Simulator, cycles int, randomState RandomState) Performance {
	for i := 0; i < cycles; i++ {
		simul.InjectState(randomState.Choose())
		simul.Tick()
	}
	return simul.Performance()
}

func MeanPerformance(preformances []Performance) Performance {
	sum := Performance{}
	for _, perf := range preformances {
		for k := range perf {
			if meanVal, exists := sum[k]; exists {
				sum[k] += meanVal
			} else {
				sum[k] = meanVal
			}
		}
	}

	for k, v := range sum {
		sum[k] = v / int64(len(preformances))
	}

	return sum
}
