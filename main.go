package main

import (
	"fmt"
	"os"
)

func main() {
	simulate := func(name string, agent Agent, env Environemt, rs RandomState) {
		fmt.Printf("%s: running...\n", name)
		fmt.Println(Simulate(
			&DefaultSimulator{
				logger: os.Stderr,
				agent:  agent,
				env:    env,
			}, 100, rs))
	}

	exercises := []struct {
		name        string
		agent       Agent
		env         Environemt
		randomState RandomState
	}{
		{
			name:  "Ex1",
			agent: &Agent1{},
			env:   &Env1{},
			randomState: RandomState{
				"a":    []interface{}{false, true},
				"b":    []interface{}{false, true},
				"door": []interface{}{false, true},
			},
		},
		{
			name:  "Ex2",
			agent: &Agent2{},
			env:   &Env2{},
			randomState: RandomState{
				"a":    []interface{}{false, true},
				"b":    []interface{}{false, true},
				"door": []interface{}{false, true},
			},
		},
		{
			name:  "Ex3",
			agent: &Agent3{},
			env:   &Env3{},
			randomState: RandomState{
				"a":    []interface{}{false, true},
				"b":    []interface{}{false, true},
				"door": []interface{}{false, true},
			},
		},
		{
			name:  "Ex4",
			agent: &Agent4{},
			env:   &Env4{},
			randomState: RandomState{
				"a":    []interface{}{false, true},
				"b":    []interface{}{false, true},
				"door": []interface{}{false, true},
			},
		},
		{
			name:  "Ex5",
			agent: &Agent5{},
			env:   &Env5{},
			randomState: RandomState{
				"a":    []interface{}{false, true},
				"b":    []interface{}{false, true},
				"door": []interface{}{false, true},
				"la":   []interface{}{false, true},
				"lb":   []interface{}{false, true},
			},
		},
		{
			name:  "Ex6",
			agent: &Agent6{},
			env:   &Env6{},
			randomState: RandomState{
				"a":    []interface{}{false, true},
				"door": []interface{}{false, true},
				"la":   []interface{}{false, true},
				"lb":   []interface{}{false, true},
			},
		},
	}

	for _, ex := range exercises {
		simulate(ex.name, ex.agent, ex.env, ex.randomState)
	}
}
