package main

type Env4 struct {
	A    bool
	door bool
}

func (e *Env4) InitState(s State) {
	e.A = s.Bool("a")
	e.door = s.Bool("door")
}

func (e *Env4) State() State {
	return State{
		"a": e.A,
	}
}

func (e *Env4) Set(action Action) {
	switch action {
	case openDoor:
		e.door = true
	case closeDoor:
		e.door = false
	case nop:
	default:
		panic("invalid action")
	}
}

type Agent4 struct {
	door  bool
	power int64

	cycles  int // Better to use time... just for testing.
	lastHit int
}

func (a *Agent4) Act(state State) Action {
	a.cycles++

	locA := state["a"].(bool)
	if locA {
		if !a.door {
			a.power++
			a.lastHit = a.cycles
			a.door = false
			return closeDoor
		}
		return nop
	}

	if a.cycles-a.lastHit >= 5 && a.door {
		a.power++
		return closeDoor
	}

	return nop
}

func (a *Agent4) Performance() Performance {
	return Performance{
		"power": a.power,
	}
}
