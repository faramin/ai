package main

import (
	"math/rand"
	"time"
)

const Env3Error = 0.3

type Env3 struct {
	A, B bool
	door bool
}

func (e *Env3) InitState(s State) {
	e.A = s.Bool("a")
	e.B = s.Bool("b")
	e.door = s.Bool("door")
}

func (e *Env3) State() State {
	return State{
		"a": e.A,
		"b": e.B,
	}
}

func (e *Env3) Set(action Action) {
	if rand.Intn(100)+1 <= 100-100*Env3Error {
		return
	}

	switch action {
	case openDoor:
		e.door = true
	case closeDoor:
		e.door = false
	case nop:
	default:
		panic("invalid action")
	}
}

type Agent3 struct {
	A, B  bool
	door  bool
	power int64
}

func (a *Agent3) Act(state State) Action {
	a.A = a.A || state["a"].(bool)
	a.B = a.B || state["b"].(bool)

	updated := false
	door := false
	if a.A && a.B {
		door = false
		updated = true
	} else if a.A || a.B {
		door = true
		updated = true
	}

	if updated && door != a.door {
		a.power++
		if door {
			return openDoor
		}
		return closeDoor
	}
	return nop
}

func (a *Agent3) Performance() Performance {
	return Performance{
		"power": a.power,
	}
}

func init() {
	rand.Seed(time.Now().Unix())
}
