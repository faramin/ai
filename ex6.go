package main

type Env6 struct {
	A      bool
	La, Lb bool
	door   bool
}

func (e *Env6) InitState(s State) {
	e.A = s.Bool("a")
	e.La = s.Bool("la")
	e.Lb = s.Bool("lb")
	e.door = s.Bool("door")
}

func (e *Env6) State() State {
	return State{
		"a":  e.A,
		"la": e.La,
		"lb": e.Lb,
	}
}

func (e *Env6) Set(action Action) {
	switch action {
	case openDoor:
		e.door = true
	case closeDoor:
		e.door = false
	case nop:
	default:
		panic("invalid action")
	}
}

type Agent6 struct {
	door  bool
	power int64
}

func (a *Agent6) Act(state State) Action {
	locA := state["la"].(bool) && state["a"].(bool)
	locB := state["lb"].(bool) && state["a"].(bool)

	if (locA || locB) && !a.door {
		a.power++
		return openDoor
	} else if !(locA || locB) && a.door {
		a.power++
		return closeDoor
	}

	return nop
}

func (a *Agent6) Performance() Performance {
	return Performance{
		"power": a.power,
	}
}
